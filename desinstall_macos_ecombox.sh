#!/bin/sh
# Ce script lance des scripts qui automatisent l'installation des éléments nécessaires 
# à l'installation de l'application e-comBox sur Mac OS

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLERREUR="\033[1;31m"  # Rouge
COLINFO="\033[0;36m"    # Cyan

ERREUR()
{
        echo -e "$COLERREUR"
        echo -e "ERREUR! Vous avez décidé de ne pas configurer e-comBox. Vous pouvez reprendre la procédure quand vous voulez"
	echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}



POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
	  echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
	  read REPONSE
          if [ -z "$REPONSE" ]; then
	     REPONSE="o"
	  fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
	   ERREUR
	fi
}


clear
echo -e "$COLTITRE"
echo "************************************************************"
echo "*            DESINSTALLATION DE L'ENVIRONNEMENT            *"
echo "************************************************************"

#Désinstallation de Docker
echo -e "$COLINFO"
echo -e "Desinstallation de Docker..."
echo -e "$COLCMD"

brew cask uninstall docker

#Suppression du dossier e-comBox
echo -e "$COLINFO"
echo -e "Suppression du dossier e-comBox..."
echo -e "$COLCMD"

rm -rf /Applications/e-comBox

#Suppression de la VM Docker
echo -e "$COLINFO"
echo -e "Nettoyage des différents fichiers..."
echo -e "$COLCMD"

rm -rf ~/Library/Application\ Scripts/com.docker.helper
rm -rf ~/Library/Caches/com.docker.docker
rm -rf ~/Library/Containers/com.docker.docker
rm -rf ~/Library/Containers/com.docker.helper

echo -e "$COLINFO"
echo -e "L'application e-comBox a été desinstallée correctement."
echo -e "$COLCMD"
exit 0